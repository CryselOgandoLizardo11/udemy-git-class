*** Setting ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     http://automationpractice.com/index.php
${scheme}       http
${prodScheme}   https
${testUrl}      ${scheme}://${homepage}
${prodUrl}      ${prodScheme}://${homepage}

*** Keywords ***
Open Homepage
    Open Browser    ${testUrl}      ${browser}

*** Test Cases ***
C001 Hacer Clic en Contenedores
    Open Homepage
    Set Global Variable     @{nombreDeContenedores}     //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR    ${Contenedores}     IN      @{nombreDeContenedores}
       Run Keyword If      '${Contenedores}'=='//*[@id="homefeatured"]/li[3]/div/div[2]/h5/a'      Exit For Loop
       Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
       Click Element       xpath=${Contenedores}
       Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
       Click Element       xpath=//*[@id="header_logo"]/a/img
    END
    Close Browser

G002 Caso de Prueba Nuevo
    Open Homepage